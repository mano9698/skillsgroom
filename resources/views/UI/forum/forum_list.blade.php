@extends('UI.base')
@section('Content')
<div id="content">
  <div class="forum_header">
  </div>
  <div class="content-wrap">
    <div class="container clearfix">
      <div id="section-price" class="section my-0 py-0" style="background: none;">
        <div class="container">
          <div class="row justify-content-between">
            <div class="col-lg-4 col-md-5 mt-4 mt-md-0 mr-0 sticky-sidebar-wrap sidebar">
              <div class="card events-meta mb-3">
                <div class="card-body topics_bg">
                  <h4>More Topics
                  </h4>
                  <ol class="keylist nobottommargin" style="line-height: 32px;">
                    @if($Questions)
                    @foreach($Questions as $Question)
                      <li>
                        <a href="/forum/details/{{$Question['id']}}/{{$Question['slug']}}">{{$Question['topic']}}
                        </a>
                      </li>
                    @endforeach
                    @else
                      <h4>No data found</h4>
                    @endif
                  </ol>
                </div>
              </div>
              <div class="image pt-5">
                <img src="images/forum/ad_forum.jpg" alt=""/>
              </div>
            </div>
            <div class="col-lg-8 postcontent nobottommargin col_last clearfix">
              <div class="single-event">
                <div class="section-title">
                    <h2>Question &amp; Answer Section
                  </h2>
                </div>
                @if($Questions)
                  @foreach($Questions as $Question)
                    <div class="card forum_bg">
                      <div class="card-body">
                        <div class="spost clearfix">
                          @if($Question['feature_img'])
                            <div class="forum-image col-3 px-0">
                              <a href="#" class="nobg">
                                
                                  <img src="https://onlinelms.skillsgroom.com/UI/questions/{{$Question['feature_img']}}" alt="" style="width: 100%; height: 100%;">
                              
                              </a>
                            </div>	
                          @else

                          @endif
                          <div class="entry-c  col-9">
                            <div class="entry-title">
                                <a href="/forum/details/{{$Question['id']}}/{{$Question['slug']}}" target="_blank">
                                    <h4 class="forum_ques">{{$Question['topic']}}
                                    </h4>
                                </a> 
                                <?php 
                                    $GetComments = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/comments_list/".$Question['id']), true);

                                    $Comments = $GetComments['data'];
                                ?>
                              <i class="forum-profile">
                                <i class="icon-calendar">
                                </i> {{date('M d Y', strtotime($Question['created_at']))}} at {{date('h:i a', strtotime($Question['created_at']))}}  |  {{$Question['first_name']}} {{$Question['last_name']}}&nbsp;&nbsp;&nbsp;
                                <i class="icon-comment">
                                </i> {{count($Comments)}} Comments
                              </i>
                            </div>
                            <p>{!! Str::words($Question['details_answer'], 30 ) !!}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach
                @else
                  <h4>No data found</h4>
                @endif
                <div class="clear">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </div>
</div>
@endsection
