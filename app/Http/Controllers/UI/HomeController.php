<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Courses;


class HomeController extends Controller
{
    public function home(){
        $title = "Home";

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list"), true);

        // echo json_encode($GetCourses['data']);
        // exit;

        $Courses = $GetCourses['data'];

        // $Courses = Courses::select('courses.id', 'admin.id as TeacherId', 'courses.course_name', 'admin.name', 'courses.type','courses.fees', 'courses.featured_image')->join('admin','admin.id', '=', 'courses.teacher_id')->where('courses.status', 1)->orderBy('courses.created_at', 'DESC')->get();

        return view('UI.home', compact('Courses'));
    }

    public function partners(){
        $title = "Home";

        return view('UI.partners');
    }

    public function book_demo($id, $course_id){
        $title = "Book Demo";

        $Courses = Courses::where('id', $course_id)->first();
        $Courses = Courses::where('id', $course_id)->first();
        return view('UI.book_demo');
    }

    public function course_list(){
        $title = "Course List";

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list"), true);

        $Courses = $GetCourses['data'];

        return view('UI.course_list', compact('Courses'));
    }


    public function course_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_details/".$id), true);

        $Courses = $GetCourses['data'];

        // echo json_encode($Courses);
        // exit;
        
        // $Courses = Courses::where('id', $id)->first();
        return view('UI.course_details', compact('Courses'));

    }
}
